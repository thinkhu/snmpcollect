export default {
  // 素材模型
  "resources_model.get_model_list": 'admin.resources_models/get_model_list',
  "resources_model.add_model": 'admin.resources_models/add_model',
  "resources_model.edit_model":"admin.resources_models/edit_model",
  "resources_model.del_model":"admin.resources_models/del_model",
  "resources_model.add_model_item":"admin.resources_models/add_model_item",
  "resources_model.edit_model_item":"admin.resources_models/edit_model_item",
  "resources_model.del_model_item":"admin.resources_models/del_model_item",
  "resources_model.refresh_model":"admin.resources_models/refresh_model",

  // 素材分类
  "resources_cate.get_list": 'admin.resources_cate/get_list',
  "resources_cate.add_cate": 'admin.resources_cate/add_cate',
  "resources_cate.edit_cate":"admin.resources_cate/edit_cate",
  "resources_cate.del_cate":"admin.resources_cate/del_cate",
  "resources_cate.refresh_cate":"admin.resources_cate/refresh_cate",

  // 素材标签
  "resources_tag.get_list": 'admin.resources_tag/get_list',
  "resources_tag.get_black_list": 'admin.resources_tag/get_black_list',
  "resources_tag.add_tag":"admin.resources_tag/add_tag",
  "resources_tag.edit_tag":"admin.resources_tag/edit_tag",
  "resources_tag.sh_tag":"admin.resources_tag/sh_tag",
  "resources_tag.del_tag":"admin.resources_tag/del_tag",
  "resources_tag.add_black_tag":"admin.resources_tag/add_black_tag",
  "resources_tag.edit_black_tag":"admin.resources_tag/edit_black_tag",
  "resources_tag.del_black_tag":"admin.resources_tag/del_black_tag",
  "resources_tag.refresh_black_tag":"admin.resources_tag/refresh_black_tag",

  // 素材
  "resources.get_list": 'admin.resources/get_list',
  "resources.menus": 'admin.resources/get_menus',
  "resources.get_detail": 'admin.resources/get_detail',
  "resources.del": 'admin.resources/del',
  "resources.edit": 'admin.resources/edit',
  "resources.edit_tag": 'admin.resources/edit_tag',
  "resources.edit_specs": 'admin.resources/edit_specs',
  "resources.batchsh": 'admin.resources/batchsh',
  "resources.batchintui": 'admin.resources/batchintui',
  "resources.batchcate": 'admin.resources/batchcate',

  // 前端视频
  "video.list": "console.video/list",
  "video.detail": "console.video/detail",
  "video.favs": "console.video/favs",
  "video.goods": "console.video/goods",
  "video.relays": "console.video/relays",
  "video.comment": "console.video/comment",
  // 前端图片
  "photo.list": "console.photo/list",
  "photo.detail": "console.photo/detail",
  "photo.favs": "console.photo/favs",
  "photo.goods": "console.photo/goods",
  "photo.relays": "console.photo/relays",
  "photo.comment": "console.photo/comment",

   // 标签
   "tags.add_resources_tag": "console.tags/add_resources_tag",
   "tags.del_resources_tag": "console.tags/del_resources_tag",
   "tags.get_resources_tag": "console.tags/get_resources_tag",
   "tags.get_tags": "console.tags/get_tags"
}