export default {
  // 配置
  "account.conf": 'cloud.config/get_config',//获取配置信息
  // // 账户注册 登陆
  // "account.login": '/account.account/login',//用户登录
  // "account.register": '/account.account/register',//用户注册
  // "account.logout":"account.account/logout",//用户登出
  // "account.send_reg_code":"account.account/send_reg_code",//发送注册验证码
  // "account.register":"account.account/register",//注册
  // "account.send_find_code":"account.account/send_find_code",//发送找回密码验证码短信
  // "account.find_pass":"account.account/find_pass",//找回密码



  // 系统模块
  // "system.get_sys_module": "/admin.system/get_sys_module",//获取模板
  // "system.get_sys_module_details": "/admin.system/get_sys_module_details",//获取模板详情
  // "system.add_sys_module": "/admin.system/add_sys_module",//添加模板
  // "system.modify_sys_module": "/admin.system/modify_sys_module",//修改模板
  // "system.delete_sys_module": "/admin.system/delete_sys_module",//删除模板
  // "system.refresh_sys_module": "/admin.system/refresh_sys_module",//刷新模板缓存
  // "system.get_sys_module_action": "/admin.system/get_sys_module_action",//获得模块方法
  // "system.save_sys_module_action": "/admin.system/save_sys_module_action",//保存模块方法

  // 权限
  // "system.get_sys_purview":"/admin.system/get_sys_purview",//获取权限角色
  // "system.add_sys_purview":"/admin.system/add_sys_purview",//添加权限角色
  // "system.modify_sys_purview":"/admin.system/modify_sys_purview",//修改权限角色
  // "system.refresh_sys_purview":"/admin.system/refresh_sys_purview",//刷新权限缓存
  // "system.delete_sys_purview":"/admin.system/delete_sys_purview",//删除权限
  // "system.get_role_purview":"/admin.system/get_role_purview",//获取用户权限模块
  // "system.save_role_purview":"admin.system/save_role_purview",//保存权限角色的模块和方法

  // 用户管理
  // "user.add_user":"admin.user/add_user",//添加用户
  // "user.get_user_list":"admin.user/get_user_list",//获取用户列表
  // "user.get_user_details":"admin.user/get_user_details",//获取用户详情
  // "user.get_userinfo":"admin.user/get_userinfo",//获取用户信息
  // "user.get_operation_log":"admin.user/get_operation_log",//获取操作日志
  // "user.get_login_log":"admin.user/get_login_log",//获取登陆日志

  // 获取导航 配置
 

 
  //队列
  "system.get_queue":"admin.system/get_queue",//获取队列列表
}
