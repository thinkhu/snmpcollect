const version = ""
export default {
  // 验证码
  "captcha": version + "/public/captcha",
  "login": version + "/public/login",
  "set_master": version + "/set_master",
  "get_userinfo":version+'/api/user/get_userinfo',

  // snmp用户管理
  "user.get_user_list": version + "/api/user/get_user_list",//获取用户列表
  "user.get_user_log": version + "/api/user/get_user_operation_log",//获取操作日志
  "user.get_user_detail": version + "/api/user/get_user_detail",//获取用户详情
  "user.add_user": version + "/api/user/add_user",//添加用户
  "user.edit_user": version + "/api/user/edit_user",//修改用户
  "user.modify_password": version + "/api/user/modify_password",//修改密码
  "user.clear_user_cache": version + "/api/user/del_user_cache",//清除用户缓存
  "user.clear_all_user_cache": version + "/api/user/clear_all_user_cache",//清除全部用户缓存

  // 集群
  "cluster.list": version + "/api/cluster/node_list",//获取集群列表
  "cluster.node_refresh": version + "/api/cluster/sync_node_uuid",//生成配置
  "cluster.node_remove": version + "/api/cluster/node_remove",//集群节点删除
  "cluster.node_save": version + "/api/cluster/node_save",//集群节点保存
  "cluster.remove": version + "/api/cluster/remove",//集群删除
  "cluster.save": version + "/api/cluster/save",//集群保存
  "cluster.get_node_uuid": version + "/api/cluster/get_node_uuid",//获取节点uuid

  // 普罗米修斯
  "prometheus.job_list": version + "/api/prometheus/job_list",//获取job列表
  "prometheus.job_save": version + "/api/prometheus/job_save",//普罗米修斯job保存
  "prometheus.job_remove": version + "/api/prometheus/job_remove",//普罗米修斯job删除
  "prometheus.job_refresh": version + "/api/prometheus/job_refresh",//生成配置
  "prometheus.target_refresh": version + "/api/prometheus/target_refresh",//生成配置
  "prometheus.target_list": version + "/api/prometheus/target_list",//获取target列表
  "prometheus.target_save": version + "/api/prometheus/target_save",//普罗米修斯target保存
  "prometheus.target_remove": version + "/api/prometheus/target_remove",//普罗米修斯target删除
  "prometheus.get_target_image": version + "/api/prometheus/get_target_image",//获取普罗米修斯target监控图
  "prometheus.get_target_image_func": version + "/api/prometheus/get_target_image_func",//获取普罗米修斯target监控图方法
  "prometheus.job_detail": version + "/api/prometheus/job_detail",//获取job详情

  // snmp模块
  "snmp_exporter.metric_list": version + "/api/snmp_exporter/metric_list",//获取snmp配置列表
  "snmp_exporter.metric_remove": version + "/api/snmp_exporter/metric_remove",//snmp配置删除
  "snmp_exporter.metric_save": version + "/api/snmp_exporter/metric_save",//snmp配置保存
  "snmp_exporter.module_list": version + "/api/snmp_exporter/module_list",//获取snmp模块列表
  "snmp_exporter.module_remove": version + "/api/snmp_exporter/module_remove",//snmp模块删除
  "snmp_exporter.module_save": version + "/api/snmp_exporter/module_save",//snmp模块保存
  "snmp_exporter.refresh": version + "/api/snmp_exporter/refresh",//生成配置
  // 用户
  // "user.userinfo": version + "/user/get_userinfo",//获取用户信息
  // "dispatch.settle.project.add": version + "/dispatch/settle/project/add",//添加/修改
  // "dispatch.settle.project.edit": version + "/dispatch/settle/project/edit",//添加/修改
  // "dispatch.settle.project": version + "/dispatch/settle/project",//列表

  // 会员管理
  // "user.get_user_list": version + "/user/get_user_list",//获取用户列表
  // "user.add_user": version + "/user/add_user", //添加用户
  // "user.edit_user": version + "/user/edit_user", // 编辑用户
  // "user.get_user_detail": version + "/user/get_user_detail", //获取用户详情
  // "user.add_user_by_tmp": version + "user/add_user_by_tmp", // 通过临时信息添加用户
  // "system.del_user_cache": version + "/user/del_user_cache", //清除缓存
  // 操作日志
  "user.get_operation_log": version + "/api/user/get_user_operation_log",//获取操作日志
  "user.get_login_log": version + "/api/user/get_user_login_log",//获取登陆日志
  // 系统管理
  "system.get_sys_config": version + "/system/get_sys_config",//获得配置
  "system.add_sys_config": version + "/system/add_sys_config",//添加配置
  "system.modify_sys_config": version + "/system/edit_sys_config",//修改配置
  "system.delete_sys_config": version + "/system/delete_sys_config",//删除配置
  //"system.get_sys_config_cate": "/admin.system/get_sys_config_cate",//配置分类
  // 联盟模块
  "system.get_sys_union":  version + "/system/get_sys_union",//获取联盟
  "system.add_sys_union":  version + "/system/add_sys_union",//添加联盟
  "system.edit_sys_union":  version + "/system/edit_sys_union",//修改联盟
  "system.delete_sys_union":  version + "/system/del_sys_union",//删除联盟
  "system.clear_cache": version + "/system/del_sys_union_cache", // 清理缓存
  // 系统模块
  "system.get_sys_module": version + "/system/get_sys_module",//获取模板
  "system.add_sys_module": version + "/system/add_sys_module",//添加配置
  "system.edit_sys_module": version + "/system/edit_sys_module",//修改配置
  "system.delete_sys_module": version + "/system/del_sys_module",//删除配置
  "system.get_sys_module_action": version + "/system/get_sys_module_action",//配置分类
  "system.save_sys_module_action": version + "/system/save_sys_module_action",//配置分类
  "system.del_sys_module_cache": version + '/system/del_sys_module_cache', //清除缓存
  'system.get_sys_module_detail': version + '/system/get_sys_module_detail', //获取模块详情
  // 权限模块
  "system.get_sys_purview": version + "/system/get_sys_purview",//获取权限角色
  "system.add_sys_purview": version + "/system/add_sys_purview",//添加权限角色
  "system.modify_sys_purview": version + "/system/edit_sys_purview",//修改权限角色
  "system.delete_sys_purview": version + "/system/del_sys_purview",//删除权限
  "system.refresh_sys_purview": version + "/system/del_sys_purview_cache",//刷新权限缓存
  "system.get_role_purview": version + "/system/get_sys_purview_action",//获取用户权限模块
  "system.save_role_purview": version + "/system/save_sys_purview_action",//保存权限角色的模块和方法
  "system.get_sys_purview_detail": version + "/system/get_sys_purview_detail", // 获取权限详情
  // 配置模块
  "system.del_sys_config_cache": version + "/system/del_sys_config_cache", //清除缓存
  "system.del_sys_config": version + "/system/del_sys_config", //删除配置

  // 组织架构
  "depart.get_depart": version + "/depart/get_depart", // 获取组织
  "depart.get_depart_count": version + "/depart/get_depart_count", // 获取组织（统计）
  "depart.sync_depart": version + "/depart/sync_depart", // 同步组织信息
  "depart.sync_quota": version + "/quota/sync_quota", // 同步配额

  // 账单统计
  "order.sync_goods_update": version + "/order/sync_goods_update", // 同步产品变更
  "order.get_ecs_order_list": version + "/order/get_ecs_order_list", // 获取ECS价格统计列表
  "order.get_ecs_order_detail": version + "/order/get_ecs_order_detail", // 获取ECS价格统计详情
  "order.get_rds_order_list": version + "/order/get_rds_order_list", // 获取RDS价格统计列表
  "order.get_rds_order_detail": version + "/order/get_rds_order_detail", // 获取RDS价格统计详情
  "order.get_oss_order_list": version + "/order/get_oss_order_list", // 获取OSS价格统计列表
  "order.get_oss_order_detail": version + "/order/get_oss_order_detail", // 获取OSS价格统计详情
  "order.get_slb_order_list": version + "/order/get_slb_order_list", // 获取SLB价格统计列表
  "order.get_slb_order_detail": version + "/order/get_slb_order_detail", // 获取SLB价格统计详情
  "order.get_redis_order_list": version + "/order/get_redis_order_list", // 获取REDIS价格统计列表
  "order.get_redis_order_detail": version + "/order/get_redis_order_detail", // 获取REDIS价格统计详情
  "order.get_order_count": version + "/order/get_order_count", // 获取产品价格统计

  // ECS
  "instance.get_ecs": version + "/instance/get_ecs", // 获取实例
  "instance.sync_aliyun_ecs": version + "/instance/sync_aliyun_ecs", // 同步阿里云ECS
  "instance.sync_zstack_instance": version + "/instance/sync_zstack_ecs", // 同步Zstack ECS
  "instance.ecs_bind_user": version + "/instance/ecs_bind_user", // 实例绑定/解绑用户
  "instance.set_ecs_up": version + "/instance/set_ecs_up", // 设置实例上传状态
  "instance.get_aliyun_ecs_disk": version + "/instance/get_aliyun_ecs_disk", // 获取阿里云磁盘
  "instance.sync_aliyun_ecs_disk": version + "/instance/sync_aliyun_ecs_disk", // 同步阿里云磁盘
  // RDS
  "instance.sync_aliyun_rds": version + "/instance/sync_aliyun_rds", // 同步阿里云RDS
  "instance.get_aliyun_rds": version + "/instance/get_aliyun_rds", // 获取阿里云RDS
  "instance.rds_bind_user": version + "/instance/rds_bind_user", // RDS绑定/解绑用户
  "instance.set_rds_up": version + "/instance/set_rds_up", // 设置实例上传状态

  // OSS
  "instance.sync_aliyun_oss": version + "/instance/sync_aliyun_oss", // 同步阿里云OSS
  "instance.get_aliyun_oss": version + "/instance/get_aliyun_oss", // 获取阿里云OOS
  "instance.oss_bind_user": version + "/instance/oss_bind_user", // OSS绑定/解绑用户
  "instance.set_oss_up": version + "/instance/set_oss_up", // 设置实例上传状态
  // SLB
  "instance.sync_aliyun_slb": version + "/instance/sync_aliyun_slb", // 同步阿里云SLB
  "instance.get_aliyun_slb": version + "/instance/get_aliyun_slb", // 获取阿里云SLB
  "instance.slb_bind_user": version + "/instance/slb_bind_user", // SLB绑定/解绑用户
  "instance.set_slb_up": version + "/instance/set_slb_up", // 设置实例上传状态
  // DEDIS
  "instance.sync_aliyun_redis": version + "/instance/sync_aliyun_redis", // 同步阿里云REDIS
  "instance.get_aliyun_redis": version + "/instance/get_aliyun_redis", // 获取阿里云REDIS

  // 设置--价格
  "order.get_price": version + "/order/get_price", // 获取价格
  "order.add_price": version + "/order/add_price", // 添加价格
  "order.edit_price": version + "/order/edit_price", // 编辑价格
  "order.del_price": version + "/order/del_price", // 删除价格
  "order.sync_price": version + "/order/sync_price", // 同步价格
  // 获取全部产品价格版本
  "order.get_all_goods_price_version": 'order/get_all_goods_price_version', // 只用作判断，不请求

  // 获取导航 配置
  "admin.get_nav": version + "/user/get_nav",
  "admin.get_cfg": version + "/user/get_config",
  "admin.modify_psd": version + '/user/modify_password',
  "admin.system_clear_cache": version + '/system/clear_cache', //清除全站缓存


}
