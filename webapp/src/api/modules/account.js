export default {
  // 账户注册 登陆
  "account.login": '/account.account/login',//用户登录
  "account.register": '/account.account/register',//用户注册
  "account.logout":"account.account/logout",//用户登出
  "account.send_reg_code":"account.account/send_reg_code",//发送注册验证码
  "account.register":"account.account/register",//注册
  "account.send_find_code":"account.account/send_find_code",//发送找回密码验证码短信
  "account.find_pass":"account.account/find_pass",//找回密码
}
