import sysApi from './sys'
import account from './account'
import web from './web'
import up from './up'
import v2 from './v2'
const APIS = Object.assign(sysApi, account, up, web, v2)

export default function (uri) {
	return APIS[uri] ? APIS[uri] : uri
}
