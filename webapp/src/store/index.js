import Vue from 'vue'
import Vuex from 'vuex'

import common from './modules/common'
import session from './modules/session'

Vue.use(Vuex)

const modules = { common, session }

const store = new Vuex.Store({
  modules,
  strict: process.env.NODE_ENV !== 'production'
})

export default store
