// header
export const IS_ADMIN_WRAP = 'IS_ADMIN_WRAP'
// 屏幕大小改变
export const SCREEN_CHANGE = 'SCREEN_CHANGE'
// 显示右边面板
export const SHOW_RIGHT_NAV = 'SHOW_RIGHT_NAV'
// 否登录
export const IS_LOGIN = 'IS_LOGIN'
// 弹出重新登录提示
export const SHOW_TOKEN_ERROR = 'SHOW_TOKEN_ERROR'
// 保存管理员信息
export const SET_USER_INFO = 'SET_USER_INFO'
// 读取配置文件
export const SET_CONFIG = 'SET_CONFIG'
