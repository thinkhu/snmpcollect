/**
 * 管理本地存储
 */
import Cache from 'web-storage-cache'
const nameSpace = 'YY:'
const lsCache = new Cache()
const ssCache = new Cache({storage: 'sessionStorage'})
import api from '@api'
const tCache = {
  set (key, value, options = null) {
    lsCache.set(nameSpace + key, value, options)
  },
  get (key) {
    return lsCache.get(nameSpace + key)
  },
  delete (key) {
    lsCache.delete(nameSpace + key)
  }
}
class CommonStorage {
  constructor (key, exp = null, type = 'localStorage') {
    this.key = nameSpace + key
    this.exp = exp
    this.storage = (type === 'localStorage') ? lsCache : ssCache
  }
  save (value) {
    const options = this.exp ? { exp: this.exp } : null
    this.storage.set(this.key, value, options)
  }
  load () {
    return this.storage.get(this.key) || ''
  }
  delete () {
    this.storage.delete(this.key)
  }
}

export function saveToken (token, expiredIn) {
  tCache.set('token', token, { exp: expiredIn })
}

export function getToken () {
  return tCache.get('token')
}

export function removeToken () {
  tCache.delete('token')
}

export const cacheUserinfo = new CommonStorage('userinfo', 0)
export const cacheConfig = new CommonStorage('config', 0)
export const cacheUnion = new CommonStorage('union', 0)
export const cacheWeb = new CommonStorage('web', 0)
export default tCache
