// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import router from './router/'
import api from '@api'
import store from '@store'
import { sync } from 'vuex-router-sync'
import '@web/style/css.css'
import '@/filter/filter'

Vue.use(VueRouter)
// 自动加载全局组件
const tplComponentTexts = require.context("@web", true, /\.tpl\.vue$/)
tplComponentTexts.keys().forEach((path) => {
  let name = ''
  path.replace('.tpl.vue', '').split('/').forEach((n) => {
    if ((/[0-9a-z]/i).test(n) && ['components', 'tpl', 'views'].indexOf(n) === -1) {
      let tname = n.toLowerCase()
      name = name + tname.slice(0,1).toUpperCase() + tname.slice(1)
    }
  })
  const componentConfig = tplComponentTexts(path)
  const c = componentConfig.default || componentConfig
  Vue.component(`v${name}`, c)
})

Vue.prototype.$api = api
sync(store, router)

require('./ElementUI')
require('@lib/common')
Vue.use(require('vue-wechat-title'))
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})