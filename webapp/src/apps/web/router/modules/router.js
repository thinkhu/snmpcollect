const Home = r => require.ensure([], () => r(require("@web/views/Home")),"Home")



const homeRoutes = {
  path: '/', name:'home', component: Home, meta: { label: '首页', auth: false, hidden: false, icon: 'fa fa-television'},
  children:[
  ]
}

export default [
  homeRoutes
]
