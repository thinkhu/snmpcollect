import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@store'

import nRoutes from './modules/router'

import {
  getToken
} from '@cache'

Vue.use(VueRouter)

const routes = [
  ...nRoutes
]

const router = new VueRouter({
  routes,
  mode: (process.env.NODE_ENV === 'development') ? 'hash' : 'hash',
  base: '/',
  history: true,
  saveScrollPosition: true,
  transitionOnLoad: true,
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})

router.beforeEach(({ meta, path, query }, from, next) => {
  const { auth = true } = meta
  const isLogin = getToken() ? true : false
  if (auth) {
    if (!isLogin) {
      store.commit(IS_LOGIN, false)
      next('/login')
    } else {
      next()
    }
  } else {
    if (isLogin && (path == '/register' || path == '/login')) {
      next('/')
    } else {
      next()
    }    
  }
})

export default router
