// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import router from './router/'
import api from '@api'
import store from '@store'
import { sync } from 'vuex-router-sync'
import '@console/style/css.css'
import '@console/style/css001.css'
import '@console/style/css002.css'
import 'font-awesome/css/font-awesome.min.css'
import '@/filter/filter'

require('@console/components')
require('./ElementUI')
require('@lib/common')
require('@admin/Videojs')
Vue.use(VueRouter)
Vue.prototype.$api = api
sync(store, router)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
