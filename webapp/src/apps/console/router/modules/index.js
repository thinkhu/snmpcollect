import { homeRoutes, loginRoutes, registerRoutes, findPass, resourcesRoutes } from './console'

export default [
  homeRoutes,
  loginRoutes,
  registerRoutes,
  findPass,
  resourcesRoutes
]
