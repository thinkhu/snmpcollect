const Home = r => require.ensure([], () => r(require('@console/views/Home')), 'Home')
const Main = r => require.ensure([], () => r(require('@console/views/main/Main')), 'Main')

const Login = r => require.ensure([],() => r(require("@console/views/account/login")),"Login")
const Register = r => require.ensure([],() => r(require("@console/views/account/Register")),"register")
const FindPass = r => require.ensure([],() => r(require("@console/views/account/FindPass")),'FindPass')

// 资源
const resourcesVideoList = r => require.ensure([], () => r(require('@console/views/resources/video/index')), 'resourcesVideoList')
const resourcesVideoCreate = r => require.ensure([], () => r(require('@console/views/resources/video/create')), 'resourcesVideoCreate')


export const loginRoutes = {
  name: 'login', path: '/login', component: Login, meta: { label: '登录', auth: false, hidden: true }
}
export const registerRoutes = {
  name: 'register', path: '/register', component: Register, meta: { label: '注册', auth: false, hidden: true }
}

export const findPass = {
  name: 'findPass', path: '/find_pass', component: FindPass, meta: { label: '找回密码', auth: false, hidden: true }
}

// 首页
export const homeRoutes = {
  path: '/', name: 'home', component: Home, redirect: '/home',  meta: { label: '首页', auth: true, hidden: true,icon:"fa fa-home" },
  children: [
    {path: '/home', name: 'main', component: Main, meta: { label: '首页', auth: true, hidden: true }}
  ]
}

export const resourcesRoutes = {
  path: '/resources', name:'resources',component: Home, redirect: '/resources/video', meta: { label: '素材库', auth: true, hidden: false, icon: 'fa fa-television'},
  children:[
    {path: "/resources/video", name:"resourcesVideo", component:resourcesVideoList, meta: { label: '视频素材', auth: true, hidden: false}},
    {path: "/resources/video/create", name:"resourcesVideoCreate", component:resourcesVideoCreate, meta: { label: '创建视频', auth: true, hidden: false}}
  ]
}