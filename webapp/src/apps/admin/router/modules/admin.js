const Home = r => require.ensure([], () => r(require('@admin/views/Home')), 'Home')
const Login =  r => require.ensure([], () => r(require('@admin/views/Login')), 'Login')
const Main = r => require.ensure([], () => r(require('@admin/views/main/Main')), 'Main')
const Err404 = r => require.ensure([], () => r(require('@admin/views/404/404')), 'Err404')
// 系统
const SysConfig = r => require.ensure([], () => r(require('@admin/views/sys/sysconfig/index')), 'SysConfig')
const SysModuleConfig = r => require.ensure([], () => r(require('@admin/views/sys/sysmodule/index')), 'SysModuleConfig')
const SysUnion = r => require.ensure([], () => r(require('@admin/views/sys/sysunion/index')),'SysUnion')
const SysPurview = r => require.ensure([], () => r(require('@admin/views/sys/syspurview/index')),'SysPurview')

// 会员管理
const MemberList = r => require.ensure([], () => r(require('@admin/views/member/memList/index')), 'MemberList')
const OperaLog = r => require.ensure([], () => r(require('@admin/views/member/operaLog/index')), 'OperaLog')
const Master = r => require.ensure([], () => r(require('@admin/views/member/master/index')), 'Master')

// const UserPsd = r => require.ensure([], () => r(require('@admin/views/member/password/index')),'UserPsd')

// snmp管理
const colony = r => require.ensure([], () => r(require('@admin/views/snmpManage/colony/index')), 'colony')
const manaIndex = r => require.ensure([], () => r(require('@admin/views/snmpManage/collection/index')), 'manaIndex')
const job = r => require.ensure([], () => r(require('@admin/views/snmpManage/collection/job/index')), 'job')
const target = r => require.ensure([], () => r(require('@admin/views/snmpManage/collection/target/index')), 'target')

// snmp出口
const metric = r => require.ensure([], () => r(require('@admin/views/snmpExporter/metric/index')), 'metric')
const module = r => require.ensure([], () => r(require('@admin/views/snmpExporter/module/index')), 'module')

// 用户
const UserList = r => require.ensure([], () => r(require('@admin/views/user/user/index')), 'UserList')
const OpLog = r => require.ensure([], () => r(require('@admin/views/user/oplog/index')),'OpLog')
const LoginLog = r => require.ensure([], () => r(require('@admin/views/user/loginlog/index')),'LoginLog')
const UserPsd = r => require.ensure([], () => r(require('@admin/views/user/password/index')),'UserPsd')

// 组织架构
const OrgHome = r => require.ensure([], () => r(require("@admin/views/organize/index")), 'OrgHome')
const OrgOrder= r => require.ensure([], () => r(require("@admin/views/organize/order/index")), 'OrgOrder')
const ECSList = r => require.ensure([], () => r(require("@admin/views/organize/hostcloud/index")), 'ECSList')
const ECSIRS = r => require.ensure([], () => r(require('@admin/views/organize/irs/index')), 'ECSIRS')
const ECSCD = r => require.ensure([], () => r(require('@admin/views/organize/cd/index')), 'ECSCD')
const SYSPrice = r => require.ensure([], ()=> r(require('@admin/views/organize/sys/index')), 'SYSPrice')
const OldResTotal = r => require.ensure([], () => r(require('@admin/views/organize/old_res_total/index')), "OldResTotal")

// 资源
const resourcesVideoList = r => require.ensure([], () => r(require('@admin/views/resources/video/index')), 'resourcesVideoList')
const resourcesVideoCreate = r => require.ensure([], () => r(require('@admin/views/resources/video/create')), 'resourcesVideoCreate')
//const resourcesPhotoList = r => require.ensure([], () => r(require('@admin/views/resources/photo/index')), 'resourcesPhotoList')
const resourcesTags = r => require.ensure([], () => r(require('@admin/views/resources/tags/index')), 'resourcesTags')
const resourcesCate = r => require.ensure([], () => r(require('@admin/views/resources/cate/index')), 'resourcesCate')
const resourcesModel = r => require.ensure([], () => r(require('@admin/views/resources/model/index')), 'resourcesModel')

// 404
export const Errors = {
  path: '/404', name: '404', component: Err404, meta: { label: '404', auth: false, hidden: true }
}
export const Err = {
  path: '*',
  redirect: '/404'
}
// 首页
export const homeRoutes = {
  path: '/', name: 'home', component: Home, redirect: '/home',  meta: { label: '首页', auth: true, hidden: true,icon:"fa fa-home" },
  children: [
    {path: '/home', name: 'main', component: Home,  meta: { label: '首页', auth: true, hidden: true }}
  ]
}
// 登录
export const loginRoutes = {
  name: 'login', path: '/login', component: Login, meta: { label: '登录', auth: false, hidden: true }
}
// export const resourcesRoutes = {
//   path: '/resources', name:'resources',component: Home, redirect: '/resources/video', meta: { label: '素材库', auth: true, hidden: false, icon: 'fa fa-television'},
//   children:[
//     {path: "/resources/video", name:"resourcesVideo", component:resourcesVideoList, meta: { label: '视频素材', auth: true, hidden: false}},
//     {path: "/resources/video/create", name:"resourcesVideoCreate", component:resourcesVideoCreate, meta: { label: '创建视频', auth: true, hidden: false}},
//     //{path: "/resources/photo", name:"resourcesPhoto", component:resourcesPhotoList, meta: { label: '图片素材', auth: true, hidden: false}}
//     {path: "/resources/tags", name:"resourcesTags", component:resourcesTags, meta: { label: '素材标签', auth: true, hidden: false}},
//     {path: "/resources/cate", name:"resourcesCate", component:resourcesCate, meta: { label: '素材分类', auth: true, hidden: false}},
//     {path: "/resources/model", name:"resourcesModel", component:resourcesModel, meta: { label: '素材模型', auth: true, hidden: false}},
//   ]
// }

// 系统
// export const sysRoutes = {
//   path:'/sys',name:'sys',component:Home,redirect:'sys/config',meta:{ label: '系统', auth: true,hidden:false,icon:'fa fa-cog'},
//   children:[
//     {path: "/sys/config",name:"sysconfig",component:SysConfig, meta: { label: '配置', auth: true,hidden:false}},
//     {path: "/sys/module",name:"sysmodule",component:SysModuleConfig,meta:{ label: '模块', auth: true,hidden:false}},
//     {path: "/sys/union",name:"sysunion",component:SysUnion, meta: { label: '联盟', auth: true,hidden:false}},
//     {path: "/sys/purview",name:"purview",component:SysPurview, meta: { label: '权限', auth: true,hidden:false}}
//   ]
// }
// 账户
export const userRoutes = {
  path: '/user',name: 'user',component: Home,redirect:'/user/psd', meta: { label: '账户管理', auth: true, hidden: true,icon:'fa fa-user-o'},
  children:[
    // {path:"/user/list",name:"userList",component:UserList,meta:{label:"会员管理",auth:true,hidden:false}},
    // {path:"/user/oplog",name:"oplog",component:OpLog,meta:{label:"操作日志",auth:true,hidden:false}},
    // {path:"/user/loginlog",name:"loginlog",component:LoginLog,meta:{label:"登陆日志",auth:true,hidden:false}},
    {path: '/user/psd', name: 'psd', component: UserPsd, meta:{label:"修改密码",auth:true,hidden:true}}
    ]
}
// 会员
export const memberRoutes = {
  path: '/member',name: 'member',component: Home,redirect:'/member/list', meta: { label: '会员管理', auth: true, hidden: false,icon:'fa fa-user-o'},
  children:[
    {path:"/member/list",name:"List",component:MemberList,meta:{label:"会员管理",auth:true,hidden:false}},
    {path:"/member/operaLog",name:"operaLog",component:OperaLog,meta:{label:"操作日志",auth:true,hidden:false}},
    {path:"/member/master", name: "master", component: Master, meta:{label:"设置主节点密码",auth:true,hidden:true}}
  ]
}

// snmp管理
export const operaRoutes = {
  path: '/snmpManage',name: 'snmpManage',component: Home,redirect:'/snmpManage/collection/target', meta: { label: '集群', auth: true, hidden: false,icon:'fa fa-user-o'},
  children:[
    {path:"/snmpManage/colony",name:"colony",component:colony,meta:{label:"集群",auth:true,hidden:false}},
    {path:"/collection",name:"manaIndex",component:manaIndex,meta:{label:"空白",auth:true,hidden:false}},
    {path:"/snmpManage/collection/job",name:"job",component:job,meta:{label:"Job",auth:true,hidden:false}},
    {path:"/snmpManage/collection/target",name:"target",component:target,meta:{label:"Target",auth:true,hidden:false,isMaster:true}},
  ]
}
// snmp出口
export const exporterRoutes = {
  path: '/snmpExporter',name: 'snmpExporter',component: Home,redirect:'/snmpExporter/module', meta: { label: 'snmp配置', auth: true, hidden: false,icon:'fa fa-user-o'},
  children:[
    {path:"/snmpExporter/module",name:"module",component:module,meta:{label:"Module",auth:true,hidden:false}},
    {path:"/snmpExporter/metric",name:"metric",component:metric,meta:{label:"Metric",auth:true,hidden:false}},
    // {path:"/snmpManage/collection",name:"collection",component:collection,meta:{label:"采集",auth:true,hidden:false}},
    // {path:"/snmpExporter/module",name:"module",component:module,meta:{label:"snmp模块",auth:true,hidden:false}},
  ]
}
// // 账号管理
// export const accountRoutes = {
//   path: '/snmpManage',name: 'snmpManage',component: Home,redirect:'/snmpManage/colony', meta: { label: '集群', auth: true, hidden: false,icon:'fa fa-user-o'},
//   children:[
//     {path:"/snmpManage/colony",name:"colony",component:colony,meta:{label:"集群",auth:true,hidden:false}},
//     {path:"/snmpManage/collection",name:"collection",component:collection,meta:{label:"采集",auth:true,hidden:false}},
//   ]
// }
// 路由部分集合
export const defalutRoutes = [
  {
    path: '/organize', name: 'org', component: Home, redirect: '/organize/statistic/home', meta: { label: '吴兴云统计平台', auth:true, hidden:false},
    children: [
      {path:"/organize/statistic/home",name:"OrgHome",component:OrgHome,meta:{label:"资源统计",auth:true,hidden:false}},
      {path:"/organize/statistic/order",name:"OrgOrder",component:OrgOrder,meta:{label:"账单统计",auth:true,hidden:false}},
      {path:"/organize/statistic/old",name:"OldResTotal",component:OldResTotal,meta:{label:"历史资源统计",auth:true,hidden:false}},
    ]
  },
  {
    path: '/organize/ecs', name: 'ecs', component: Home, redirect: '/organize/product/ecs/list', meta: { label: 'ECS', auth:true, hidden:false},
    children: [
      {path:"/organize/product/ecs/list",name:"ECSIRS",component:ECSIRS,meta:{label:"云主机",auth:true,hidden:false}},
      {path:"/organize/product/ecs/cd",name:"ECSCD",component:ECSCD,meta:{label:"云盘",auth:true,hidden:false}},
    ]
  },
  {
    path: '/organize/rds', name: 'rds', component: Home, redirect: '/organize/product/rds/list', meta: { label: 'RDS', auth:true, hidden:false},
    children: [
      {path:"/organize/product/rds/list",name:"RDSList",component:ECSIRS,meta:{label:"云数据库",auth:true,hidden:false}},
    ]
  },
  {
    path: '/organize/product/oss', name: 'oss', component: Home, redirect: '/organize/product/oss/list', meta: { label: 'OSS', auth:true, hidden:false},
    children: [
      {path:"/organize/product/oss/list",name:"OSSList",component:ECSIRS,meta:{label:"对象存储",auth:true,hidden:false}},
    ]
  },
  {
    path: '/organize/product/slb', name: 'slb', component: Home, redirect: '/organize/product/slb/list', meta: { label: 'SLB', auth:true, hidden:false},
    children: [
      {path:"/organize/product/slb/list",name:"SlBList",component:ECSIRS,meta:{label:"负载均衡",auth:true,hidden:false}},
    ]
  },
  {
    path: '/organize/product/redis', name: 'redis', component: Home, redirect: '/organize/product/redis/list', meta: { label: 'REDIS', auth:true, hidden:false},
    children: [
      {path:"/organize/product/redis/list",name:"REDISList",component:ECSIRS,meta:{label:"远程字典服务",auth:true,hidden:false}},
    ]
  },
  // 设置
  {
    path: '/organize/sys', name: 'sys', component: Home, redirect: '/organize/sys/prices', meta: { label: 'ECS', auth:true, hidden:false},
    children: [
      {
        path:"/organize/sys/prices",name:"sysPrice",component: SYSPrice, meta:{ label:"产品价格",auth:true,hidden:false },
      }
    ]
  }
]
// 账户
const accountRoutes = {

}
// 订单
const orderRoutes = {
}
