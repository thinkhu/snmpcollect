import {Err, Errors, homeRoutes, memberRoutes,operaRoutes,loginRoutes,exporterRoutes, sysRoutes, userRoutes, resourcesRoutes, defalutRoutes } from './admin'

export default [
  Err,
  Errors,
  homeRoutes,
  memberRoutes,
  operaRoutes,
  exporterRoutes,
  loginRoutes,
  userRoutes,
  // sysRoutes,
  // resourcesRoutes,
  ...defalutRoutes
]
