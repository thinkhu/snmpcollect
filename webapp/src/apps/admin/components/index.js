import Vue from 'vue'
export function autoload () {
	const tplComponentTexts = require.context("@admin", true, /\.tpl\.vue$/)
	tplComponentTexts.keys().forEach((path) => {
	  let name = ''
	  path.replace('.tpl.vue', '').split('/').forEach((n) => {
	  	if ((/[0-9a-z]/i).test(n) && ['components', 'tpl', 'views'].indexOf(n) === -1) {
        let tname = n.toLowerCase()
	      name = name + tname.slice(0,1).toUpperCase() + tname.slice(1)
	  	}
	  })
	  const componentConfig = tplComponentTexts(path)
	  const c = componentConfig.default || componentConfig
	  Vue.component(`v${name}`, c)
	})
}
autoload()
