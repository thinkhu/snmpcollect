import Vue from 'vue'
import {cacheConfig} from '@cache'
// 数组操作
Array.prototype.remove = function(val) {
	var index = this.indexOf(val);
	if (index > -1) {
		this.splice(index, 1);
	}
}
Array.prototype.find_key_val = function(k, v) {
  for (let i = 0; i < this.length; i++) {
    if (this[i][k]  == v) {
      return i
    }
  }
  return -1
}
// 交换数组位置
Array.prototype.swap = function(beforeindex, afterindex) {
	this[beforeindex] = this.splice(afterindex, 1, this[beforeindex])[0]
}
// 下移
Array.prototype.down = function (index){
   if(index + 1 != this.length){
       this.swap(index, index + 1)
   }
}
// 上移
Array.prototype.up = function (index){
   if(index != 0){
       this.swap(index, index - 1)
   }
}
//置顶，即将当前元素移到数组的第一位
Array.prototype.top = function (index){
   this.unshift(this.splice(index, 1))
}

//置底，即将当前元素移到数组的最后一位
Array.prototype.end = function (index){
   this.push(this.splice(index, 1))
}

Vue.prototype.$config = function (value) {
  let config = cacheConfig.load()
  let re = null 

  if (value && config) {
    let p = value.split('.')
    if (config[p[0]]) {
      re = config[p[0]]
      if (p[1]) {
        re = re[p[1]]
      }
    }
  }  
  return re
}