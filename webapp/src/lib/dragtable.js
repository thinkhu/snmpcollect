export default (Vue) => {
  Vue.directive("thdrag", {
    bind:(el)=> {
      let op = el
      let obj = null
      let table = null
      let dis = 20
      op.onmousedown = (e) => {
        //记录单元格
        obj = el
        if (e.offsetX > op.offsetWidth - dis) {
          obj.mouseDown = true
          obj.oldX = e.x
          obj.oldWidth = op.offsetWidth
          console.log('drag start')
        }
        //记录Table宽度
        //table = obj; while (table.tagName != ‘TABLE') table = table.parentElement;
        //obj.tableWidth = table.offsetWidth;
      }
      op.onmouseup = function (e) {
        //结束宽度调整
        if (obj == undefined) obj = op
        obj.mouseDown = false
        obj.style.cursor = 'default'
        console.log(obj)
        console.log('drag end')
      }
      op.onmousemove = function (e) {
        if (obj == undefined) obj = op
        //更改鼠标样式
        if (e.offsetX > op.offsetWidth - dis) {
          op.style.cursor = 'col-resize'
          console.log(obj.mouseDown)
          if (obj.mouseDown != null && obj.mouseDown === true) {
            if (obj.oldWidth + (e.x - obj.oldX) > 0)
              obj.width = obj.oldWidth + (e.x - obj.oldX)
            //调整列宽
            obj.style.width = obj.width            
          }
        } else {
          op.style.cursor = 'default'
          obj.mouseDown = false
        }        
      }
    }
  })
}
